#!/bin/sh

set -e

: DEFAULT_BASE_URI=${DEFAULT_BASE_URI:=https://www.bmoine.fr}
: BASE_URI=${BASE_URI:=http://localhost}
: WEBROOT_PATH=${WEBROOT_PATH:=./data/www/webroot/}

printf "Replace %s by %s?\n" "${DEFAULT_BASE_URI}" "${BASE_URI}"

read -p "(y/n) >>> " CHOICE

if [ "${CHOICE}" = "y" ] || [ "${CHOICE}" = "Y" ]; then
    find ${WEBROOT_PATH} -type f \( -name "*.html" -o -name "*.js" -o -name "*.css" -o -name "*.php" -o -name "*.md" \) -exec perl -p -i -e 's@'${DEFAULT_BASE_URI}'@'${BASE_URI}'@g' {} \;
else
	printf "%b" "
Usage:
DEFAULT_BASE_URI=${DEFAULT_BASE_URI} BASE_URI=${BASE_URI} WEBROOT_PATH=${WEBROOT_PATH} ${0}
"
fi

